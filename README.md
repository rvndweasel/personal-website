# My Personal Website

This repository contains the source code for my personal website, showcasing my background in computer science, programming, network administration, and system administration.

## Structure

The website consists of three main pages:

1. Home Page: A brief introduction to me and my background.
2. Resume Page: An HTML version of my professional resume.
3. Articles Page: A collection of articles, primarily focused on tutorials and documentation.

## Installation

This website is built using HTML, CSS, and vanilla JavaScript, and it requires no specific installation. You can access it through any modern web browser.

## Usage

Simply visit [oncken.dev](https://oncken.dev/) to start exploring the world of technology. Navigate through the various sections and click on articles, tutorials, and other content to learn and discover.

## Technologies

- HTML
- CSS
- JavaScript

## Contact

For any questions, feedback, or collaborations, you can reach me at:
- Email: cody@oncken.dev
