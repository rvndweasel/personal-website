document.addEventListener("DOMContentLoaded", function() {
    const tabs = document.querySelectorAll('input[name="tabs"]');
    tabs.forEach(tab => {
        tab.addEventListener('change', function() {
            const contentId = 'content' + this.id.slice(-1); // Assumes tab1, tab2, tab3
            const contents = document.querySelectorAll('.tab-content');
            contents.forEach(content => {
                content.style.display = 'none'; // Hide all contents
            });
            document.getElementById(contentId).style.display = 'block'; // Show selected content
        });
    });
    // Automatically select the first tab on page load
    tabs[0].checked = true;
    tabs[0].dispatchEvent(new Event('change'));
});