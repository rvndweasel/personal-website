// Global variables
let articles = [];
const articlesPerPage = 5;
let currentPage = 1;
let currentTag = 'all';

// Fetch articles from JSON file
fetch('assets/js/articles.json')
    .then(response => response.json())
    .then(data => {
        articles = data.articles;
        displayArticles();
    })
    .catch(error => console.error('Error loading articles:', error));

function displayArticles() {
    const container = document.getElementById('articles-container');
    if (!container) {
        console.error('Articles container not found');
        return;
    }
    container.innerHTML = ''; // Clear existing articles

    const startIndex = (currentPage - 1) * articlesPerPage;
    const endIndex = startIndex + articlesPerPage;
    const filteredArticles = articles.filter(article => 
        currentTag === 'all' || article.tags.includes(currentTag)
    );
    const articlesToShow = filteredArticles.slice(startIndex, endIndex);

    articlesToShow.forEach(article => {
        const articleElement = document.createElement('article');
        articleElement.className = 'card layered-box';
        articleElement.setAttribute('data-tag', article.tags.join(','));
        articleElement.innerHTML = `
            <div class="card-media"></div>
            <div class="card-content">
                <h2 class="card-title">${article.title}</h2>
                <p class="card-date">Published on ${article.date}</p>
                <p class="card-summary">${article.summary}</p>
                <a href="${article.link}" class="card-read-more">Read More</a>
            </div>
        `;
        container.appendChild(articleElement);
    });

    updatePagination(filteredArticles.length);
}

function updatePagination(totalArticles) {
    const totalPages = Math.ceil(totalArticles / articlesPerPage);
    const paginationElement = document.getElementById('pagination');
    if (!paginationElement) {
        console.error('Pagination container not found');
        return;
    }
    paginationElement.innerHTML = '';

    if (totalPages > 1) {
        for (let i = 1; i <= totalPages; i++) {
            const pageButton = document.createElement('button');
            pageButton.textContent = i;
            pageButton.classList.add('pagination-button');
            if (i === currentPage) {
                pageButton.classList.add('active');
            }
            pageButton.addEventListener('click', () => {
                currentPage = i;
                displayArticles();
            });
            paginationElement.appendChild(pageButton);
        }
    }
}

function filterArticles(tag) {
    currentTag = tag;
    currentPage = 1; // Reset to first page when filtering
    displayArticles();
}

// Make filterArticles function globally accessible
window.filterArticles = filterArticles;