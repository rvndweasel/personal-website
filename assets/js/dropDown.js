document.addEventListener('click', function (e) {
    const isDropdownTrigger = e.target.matches(".custom-select-trigger");
    if (isDropdownTrigger) {
        var customSelect = e.target.parentElement;
        customSelect.classList.toggle('open');
    }

    // Close the dropdown if clicked outside
    if (!e.target.closest('.custom-select')) {
        document.querySelectorAll('.custom-select').forEach(function (select) {
            select.classList.remove('open');
        });
    }

    const isOption = e.target.matches(".custom-option");
    if (isOption) {
        let option = e.target;
        let customSelect = option.closest('.custom-select');
        let trigger = customSelect.querySelector('.custom-select-trigger');
        let options = customSelect.querySelectorAll('.custom-option');

        options.forEach(function (otherOption) {
            otherOption.classList.remove('selected');
        });

        option.classList.add('selected');
        trigger.textContent = option.textContent;


        filterArticles(option.getAttribute('data-value'));
        customSelect.classList.remove('open');
    }
});

// Close the dropdown if the user clicks outside of it
window.addEventListener('click', function (e) {
    const withinBoundaries = e.composedPath().includes(document.querySelector('.custom-select'));
    if (!withinBoundaries) {
        document.querySelectorAll('.custom-select').forEach(function (select) {
            select.classList.remove('open');
        });
    }
});