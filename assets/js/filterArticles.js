function filterArticles(tag) {
    const articles = document.querySelectorAll('.card');

    articles.forEach(article => {
        // Assume each card has a data-tag attribute with comma-separated tags
        const articleTags = article.getAttribute('data-tag').split(',');

        if (tag === 'all' || articleTags.includes(tag)) {
            // Show the article
            article.style.display = '';
        } else {
            // Hide the article
            article.style.display = 'none';
        }
    });
}